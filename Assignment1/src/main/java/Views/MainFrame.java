package Views;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MainFrame extends JFrame{
    
    JPanel panel = new JPanel();
    JLabel firstPolyLabel = new JLabel("First Polynomial:");
    JTextField firstPolyField = new JTextField();
    JLabel secondPolyLabel = new JLabel("Second Polynomial:");
    JTextField secondPolyField = new JTextField();
    JLabel resultLabel = new JLabel("Result:");
    JTextField resultField = new JTextField();
    JButton addButton = new JButton("ADD ( + )");
    JButton substractButton = new JButton("SUB ( - )");
    JButton multiplyButton = new JButton("MUL ( * )");
    JButton divideButton = new JButton("DIV ( / )");
    JButton derivateButton = new JButton("DER ( ' )");
    JButton integrateButton = new JButton("INT ( S )");
    
    Dimension dimension = new Dimension(430, 230);
    
    public MainFrame()
    {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        panel.setPreferredSize(dimension);
        panel.setLayout(null);
        this.add(panel);
        pack();
        
        firstPolyLabel.setBounds(70, 10, 140, 20);
        firstPolyField.setBounds(215, 10, 140, 20);
        secondPolyLabel.setBounds(70, 40, 140, 20);
        secondPolyField.setBounds(215, 40, 140, 20);
        resultLabel.setBounds(70, 70, 140, 20);
        resultField.setBounds(215, 70, 140, 20);
        addButton.setBounds(40, 100, 140, 20);
        substractButton.setBounds(40, 130, 140, 20);
        multiplyButton.setBounds(40, 160, 140, 20);
        divideButton.setBounds(240, 100, 140, 20);
        derivateButton.setBounds(240, 130, 140, 20);
        integrateButton.setBounds(240, 160, 140, 20);
        
        panel.add(firstPolyLabel);
        panel.add(firstPolyField);
        panel.add(secondPolyLabel);
        panel.add(secondPolyField);
        panel.add(resultLabel);
        panel.add(resultField);
        panel.add(addButton);
        panel.add(substractButton);
        panel.add(multiplyButton);
        panel.add(divideButton);
        panel.add(integrateButton);
        panel.add(derivateButton);
        
    }
    
    public String getFirstPolynomial()
    {
        return firstPolyField.getText();
    }
    
    public String getSecondPolynomial()
    {
        
        return secondPolyField.getText();
    }
    
    public void setResultValue(String result)
    {
        resultField.setText(result);
    }
     
    public void addButtonAction( ActionListener a)
    {
        addButton.addActionListener(a);
    }
    
    public void substractButtonAction( ActionListener a)
    {
        substractButton.addActionListener(a);
    }
    
    public void multiplyButtonAction( ActionListener a)
    {
        multiplyButton.addActionListener(a);
    }
    
    public void divideButtonAction( ActionListener a)
    {
        divideButton.addActionListener(a);
    }
    
    public void derivateButtonAction( ActionListener a)
    {
        derivateButton.addActionListener(a);
    }
    
    public void integrateButtonAction( ActionListener a)
    {
        integrateButton.addActionListener(a);
    }
    
}