package Business;

import Models.*;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class Operations{
    
    public static Polynomial add(Polynomial pol1, Polynomial pol2)
    {
        ArrayList<Monom> polyn1 = pol1.getMonoms();
        ArrayList<Monom> polyn2 = pol2.getMonoms();
        
        for(Monom m2 : polyn2)
                Polynomial.addMonom(m2, polyn1);
        
        Polynomial polRez = new Polynomial(polyn1);
        polRez.sort();
        
        return polRez;
        
    }
    
    public static Polynomial substract(Polynomial pol1, Polynomial pol2)
    {
        ArrayList<Monom> polyn1 = pol1.getMonoms();
        ArrayList<Monom> polyn2 = pol2.getMonoms();
        
        for(Monom m2 : polyn2)
        {
            m2.setCoeficient(-m2.getCoeficient());
            Polynomial.addMonom(m2, polyn1);
        }
        
        Polynomial polRez = new Polynomial(polyn1);
        polRez.sort();
        
        return polRez;
    }
    
    public static Polynomial multiply(Polynomial pol1, Polynomial pol2)
    {
        ArrayList<Monom> polyn1 = pol1.getMonoms();
        ArrayList<Monom> polyn2 = pol2.getMonoms();
        ArrayList<Monom> rez = new ArrayList<Monom>();
        
        for(Monom m1 : polyn1)
        for(Monom m2 : polyn2)
        {
            Polynomial.addMonom(new Monom(m1.getCoeficient()*m2.getCoeficient(),m1.getPower()+m2.getPower()), rez);
        }
        
        Polynomial polRez = new Polynomial(rez);
        polRez.sort();
        
        return polRez;
    }
    
    public static Polynomial derivate(Polynomial pol1)
    {
        ArrayList<Monom> polyn1 = pol1.getMonoms();
        ArrayList<Monom> rez = new ArrayList<Monom>();
        
        for(Monom m1 : polyn1)
        {   
            if(m1.getPower() >= 0)
            {
                rez.add(new Monom(m1.getCoeficient()*m1.getPower(), m1.getPower()-1));
            }
        }
        
        Polynomial polRez = new Polynomial(rez);
        polRez.sort();
        
        return polRez;
    }
    
    public static Polynomial integrate(Polynomial pol1)
    {
        ArrayList<Monom> polyn1 = pol1.getMonoms();
        ArrayList<Monom> rez = new ArrayList<Monom>();
        
        double newCoeficient;
        int newPower;
        
        for(Monom m1 : polyn1)
        {
            newCoeficient = m1.getCoeficient()/(m1.getPower()+1);
            newPower = m1.getPower()+1;
            
            rez.add(new Monom(newCoeficient, newPower));
            
        }
            
        Polynomial polRez = new Polynomial(rez);
        polRez.sort();
        
        return polRez;
    }
    
    public static Monom divideMonoms(Monom m1, Monom m2)
    {
    	if(m1.getPower()-m2.getPower()>=0)
    	return new Monom((long)m1.getCoeficient()/(long)m2.getCoeficient(),m1.getPower()-m2.getPower());
    	else return new Monom();
    }
    
    public static Polynomial divide(Polynomial pol1, Polynomial pol2)
    {
    	Polynomial rez = new Polynomial();
		Polynomial aux = new Polynomial();
	
		Polynomial pol2Copy = new Polynomial();
		pol2Copy = pol2;
		
		try {
		
		while(pol2.getDominantTerm().getPower() <= pol1.getDominantTerm().getPower() && divideMonoms(pol1.getDominantTerm(),pol2.getDominantTerm()).getCoeficient()!=0)
		{
			
			aux.addElement(divideMonoms(pol1.getDominantTerm(),pol2.getDominantTerm()));
			
			rez=add(rez,aux);
			
			pol2Copy = multiply(pol2,aux);
			
			pol1 = substract(pol1,pol2Copy);
			
			aux=new Polynomial();
		}
	
    	return rez;}catch(Exception ex) {
    		JOptionPane.showMessageDialog(null, "Invalid input! Q.Q");
    		return rez;
    	}
    }
    
}