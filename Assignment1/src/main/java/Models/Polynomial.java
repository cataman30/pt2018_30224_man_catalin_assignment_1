package Models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.print.attribute.standard.MediaSize.Other;

public class Polynomial{

    ArrayList<Monom> polyn = new ArrayList<Monom>();
    
    public Polynomial()
    {
    	this.polyn.clear();
    }
    
    public Polynomial( String poly )
    {
    	this.polyn.clear();
    	Pattern p = Pattern.compile( "(-?\\b\\d+)[xX]\\^(-?\\d+\\b)" );
        Matcher matcher = p.matcher(poly);
    
         while (matcher.find())
        {
            this.addMonom(new Monom(Double.parseDouble(matcher.group(1)),Integer.parseInt(matcher.group(2))),polyn);
        }
         
        this.sort();
    }
    
    public Polynomial(ArrayList<Monom> array)
    {
        this.polyn = array;
    }
    
    public String toString()
    {
        String output = new String();
        
        for(Monom m: polyn)
            output = output + m.toString();
        
        return output;
    }
    
    
    public void setMonomCoefAndPower(double coef, int power, int index)
    {
        this.polyn.set(index, new Monom(coef, power));
    }
    
    public double getMonomCoef(int index)
    {
        return polyn.get(index).getCoeficient();
    }
    
    public int getMonomPower(int index)
    {
        return polyn.get(index).getPower();
    }
    
    public int getLength()
    {
        return polyn.size();
    }
            
    public void addElement(Monom m)
    {
        polyn.add(m);
    }
    
    public static void addMonom(Monom m, ArrayList<Monom> array)
    {
        boolean ok = false;
        for(Monom iter : array)
        {
            if(m.getPower() == iter.getPower())
            {
                iter.setCoeficient(m.getCoeficient()+iter.getCoeficient());
                ok = true;
            }
        }
        
        if(ok==false)
        {
            array.add(m);
        }
    }
    
    public Monom getMonom(int index)
    {
        return polyn.get(index);
    }
    
    public ArrayList<Monom> getMonoms()
    {
        return polyn;
    }
    
    public void sort()
    {
        Collections.sort(polyn);         
    }

    
    public Monom getDominantTerm()
    {
    	Monom mrez = new Monom();
    	
    	for(Monom iter : polyn)
    	{
    		if(iter.getPower()>mrez.getPower() && iter.getCoeficient()!=0)
    			mrez=iter;
    	}
    	
    	return mrez;
    		
    }
}