package Models;

import java.text.DecimalFormat;

public class Monom implements Comparable<Monom>{
    
    private double coeficient;
    private int power;
    
    public Monom()
    {
        this.coeficient = 0;
        this.power = 0;
    }
    
    public Monom(double coeficient, int power)
    {
        this.coeficient = coeficient;
        this.power = power;
    }
    
    public void setCoeficient(double coeficient)
    {
        this.coeficient = coeficient;
    }
    
    public void setPower(int power)
    {
        this.power = power;
    }
    
    public double getCoeficient()
    {
        return this.coeficient;
    }
    
    public int getPower()
    {
        return this.power;
    }
    
    public String toString()
    {
    	DecimalFormat df = new DecimalFormat();
    	df.setMaximumFractionDigits(2);
    	String prtCoef = new String();
        prtCoef = (long) this.coeficient == this.coeficient ? "" + (long) this.coeficient : "" + df.format(this.coeficient);
        String output = new String();
        
        if(prtCoef.compareTo("0")!=0)
        if(this.power>1)
            {
                if(this.coeficient > 0)
                    output=output+"+";
                
                if(prtCoef.compareTo("1")==0)
                    output=output+"x^"+this.power;
                else
                output=output+prtCoef+"x^"+this.power;
            }
        else
            if(this.power == 1)
            {
                if(this.coeficient > 0)
                    output=output+"+";
                
                if(prtCoef.compareTo("1")==0)
                    output=output+"x";
                else
                output=output+prtCoef+"x";
            }
            else
            {
                if(this.coeficient!=0)
                {
                    if(this.coeficient > 0)
                    output=output+"+";
                output=output+prtCoef;
                }
               
            }
        
       
        	return output;
     
    }
    
    
    @Override
    public int compareTo(Monom m) {
        if(this.getPower()<m.getPower())
            return 1;
        else
            if(this.getPower()==m.getCoeficient())
                return -1;
            else 
                return -1;
    }
}