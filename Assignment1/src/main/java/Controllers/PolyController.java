package Controllers;
import Models.*;
import Views.*;
import Business.Operations;

public class PolyController {
    
    
    
    public static void initialise() {
       
       MainFrame view = new MainFrame();
       view.setVisible(true);
       
       view.addButtonAction(a->{
       
           Polynomial pol1 = new Polynomial(view.getFirstPolynomial());
           Polynomial pol2 = new Polynomial(view.getSecondPolynomial());
           view.setResultValue(Operations.add(pol1, pol2).toString());
       
       });
       
        view.substractButtonAction(a->{
       
           Polynomial pol1 = new Polynomial(view.getFirstPolynomial());
           Polynomial pol2 = new Polynomial(view.getSecondPolynomial());
           view.setResultValue(Operations.substract(pol1, pol2).toString());
       
       });
        
         view.multiplyButtonAction(a->{
       
           Polynomial pol1 = new Polynomial(view.getFirstPolynomial());
           Polynomial pol2 = new Polynomial(view.getSecondPolynomial());
           view.setResultValue(Operations.multiply(pol1, pol2).toString());
       
       }); 
       
         view.derivateButtonAction(a->{
             
            Polynomial pol1 = new Polynomial(view.getFirstPolynomial());
            view.setResultValue(Operations.derivate(pol1).toString());
             
         });
         
         view.integrateButtonAction(a->{
             
            Polynomial pol1 = new Polynomial(view.getFirstPolynomial());
            view.setResultValue(Operations.integrate(pol1).toString());
             
         });
       
         view.divideButtonAction(a->{
             
             Polynomial pol1 = new Polynomial(view.getFirstPolynomial());
             Polynomial pol2 = new Polynomial(view.getSecondPolynomial());
             view.setResultValue(Operations.divide(pol1,pol2).toString()+"    REST: "+pol1);
              
          });
         
    }
    
}
