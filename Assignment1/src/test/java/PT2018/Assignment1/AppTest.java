package PT2018.Assignment1;

import Business.Operations;
import Models.Polynomial;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
    
    public void testSum()
    {
    	Polynomial pol1 = new Polynomial("12x^4-6x^3+2x^1+7x^0");
    	Polynomial pol2 = new Polynomial("3x^2+2x^0");
    	Polynomial polRez = new Polynomial();
    	Polynomial expected = new Polynomial("12x^4-6x^3+3x^2+2x^1+9x^0");
    	
    	polRez = Operations.add(pol1,pol2);
    	
    	assertTrue(polRez.toString().equals(expected.toString()));
    	
    }
    
    public void testSubstraction()
    {
    	Polynomial pol1 = new Polynomial("12x^4-6x^3+2x^1+7x^0");
    	Polynomial pol2 = new Polynomial("3x^2+2x^0");
    	Polynomial polRez = new Polynomial();
    	Polynomial expected = new Polynomial("12x^4-6x^3-3x^2+2x^1+5x^0");
    	
    	polRez = Operations.substract(pol1,pol2);
    	
    	assertTrue(polRez.toString().equals(expected.toString()));
    	
    }
    
    public void testMultiply()
    {
    	Polynomial pol1 = new Polynomial("12x^4-6x^3+2x^1+7x^0");
    	Polynomial pol2 = new Polynomial("3x^2+2x^0");
    	Polynomial polRez = new Polynomial();
    	Polynomial expected = new Polynomial("36x^6-18x^5+24x^4-6x^3+21x^2+4x^1+14x^0");
    	
    	polRez = Operations.multiply(pol1,pol2);
    	
    	assertTrue(polRez.toString().equals(expected.toString()));
    	
    }
    
    public void testDifferenciation()
    {
    	Polynomial pol1 = new Polynomial("12x^4-6x^3+2x^1+7x^0");
    	Polynomial polRez = new Polynomial();
    	Polynomial expected = new Polynomial("48x^3-18x^2+2x^0");
    	
    	polRez = Operations.derivate(pol1);
    	
    	assertTrue(polRez.toString().equals(expected.toString()));
    	
    }
    
    public void testIntegration()
    {
    	Polynomial pol1 = new Polynomial("4x^3+6x^2+24x^1");
    	Polynomial polRez = new Polynomial();
    	Polynomial expected = new Polynomial("1x^4+2x^3+12x^2");
    	
    	polRez = Operations.integrate(pol1);
    	
    	assertTrue(polRez.toString().equals(expected.toString()));
    	
    }
    
    
    public void testDivision()
    {
    	Polynomial pol1 = new Polynomial("12x^4-6x^3+2x^1+7x^0");
    	Polynomial pol2 = new Polynomial("3x^2+2x^0");
    	
    	assertTrue((Operations.divide(pol1,pol2).toString()+"    REST: "+pol1).equals("+4x^2-2x-2    REST: -2x^2+6x+11"));
    	
    }
    
    
}
